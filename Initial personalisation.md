These are the steps needed to personalize the [main project](https://gitlab.com/pages/plain-html) for your own fork on GitLab.com.

1. [x] Create a new branch named "**live**" and set it as the default branch via <kbd><var>Settings -> Repository -> Default Branch</var></kbd>  
	We will be working on this branch from now on, so we won't need to break the fork relationship with original.
1. [ ] Adjust some Project settings via:
	1. <kbd><var>Settings -> General -> Advanced -> Rename repository</var></kbd>
		- Change **Project name** to any name you like.
		- [ ] Change **Path** to <kbd><var>`*.gitlab.io`</var></kbd> with <var>`*`</var> replaced by your **project groupname** or **username**.
	1. <kbd><var>Settings -> General -> General project</var></kbd>
		- Change **Project description (optional)** to anything you like.
		- Update **Project avatar** with any image you like.
	1. <kbd><var>Settings -> General -> Permissions</var></kbd> and  
<kbd><var>Settings -> General -> Merge request</var></kbd>
		- Change any settings to suit your needs.
	1. [ ] <kbd><var>Settings -> General -> Badges</var></kbd>  
	These badges will be shown on the project's main page.  
	*(Not needed when already setup in group settings incase of projects inside a group)*
		1. [ ] Create a *Pipeline* badge
			- Set **Link** to <kbd><var>`https://gitlab.com/%{project_path}/commits/%{default_branch}`</var></kbd>
			- Set **Badge image URL** to <kbd><var>`https://gitlab.com/%{project_path}/badges/%{default_branch}/build.svg`</var></kbd>
		1. [ ] Create a *Coverage* badge
			- Set **Link** to <kbd><var>`https://gitlab.com/%{project_path}/pipelines`</var></kbd>
			- Set **Badge image URL** to <kbd><var>`https://gitlab.com/%{project_path}/badges/%{default_branch}/coverage.svg`</var></kbd>
	1. [ ] Configure and control [Auto DevOps](https://gitlab.com/help/topics/autodevops/index.md):
		1. <kbd><var>Settings -> CI/CD -> Auto DevOps</var></kbd>
			- [ ] Adjust **Deployment strategy** to suit your needs.  
			*Preferably select **Automatic deployment to staging, manual deployment to production***.
		1. [ ] Optionally update <kbd><var>`.gitlab-ci.yml`</var></kbd>  
		see also:
			- [Auto-DevOps.gitlab-ci.yml](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/lib/gitlab/ci/templates/Auto-DevOps.gitlab-ci.yml) for an example.
			- [Notes about autodeploy](https://gitlab.com/help/topics/autodevops/index.md#auto-deploy)
			- [Yaml Readme](https://gitlab.com/help/ci/yaml/README) for Configuration of your jobs with <kbd><var>`.gitlab-ci.yml`</var></kbd>.
		1. <kbd><var>Settings -> CI/CD -> Variables</var></kbd> create these [variables](https://gitlab.com/help/ci/variables/README.md#variables):
			- [ ] Set <kbd><var>`AUTO_DEVOPS_DOMAIN`</var></kbd> to <kbd><var>`*.gitlab.io`</var></kbd> with <var>`*`</var> replaced by your **project groupname** or **username**.  
			This will set the domain under which your [GitLab Pages](https://gitlab.com/help/user/project/pages/index.md) website will be accessible from the internet.
			- [ ] Set <kbd><var>`STAGING_ENABLED`</var></kbd> to <kbd><var>`1`</var></kbd>.
			- [ ] Set <kbd><var>`CANARY_ENABLED`</var></kbd> to <kbd><var>`1`</var></kbd>.
			- [ ] Set <kbd><var>`INCREMENTAL_ROLLOUT_MODE`</var></kbd> to <kbd><var>`manual`</var></kbd>.
			- [ ] Optionally set <kbd><var>`INCREMENTAL_ROLLOUT_ENABLED`</var></kbd> to <kbd><var>`1`</var></kbd>.
			- [ ] Optionally set <kbd><var>`CODE_QUALITY_DISABLED`</var></kbd> to <kbd><var>`1`</var></kbd> to disable this feature.
			- [ ] Optionally set <kbd><var>`CONTAINER_SCANNING_DISABLED`</var></kbd> to <kbd><var>`1`</var></kbd> to disable this feature.
			- [ ] Optionally set <kbd><var>`DAST_DISABLED`</var></kbd> to <kbd><var>`1`</var></kbd> to disable this feature.
			- [ ] Optionally set <kbd><var>`DEPENDENCY_SCANNING_DISABLED`</var></kbd> to <kbd><var>`1`</var></kbd> to disable this feature.
			- [ ] Optionally set <kbd><var>`LICENSE_MANAGEMENT_DISABLED`</var></kbd> to <kbd><var>`1`</var></kbd> to disable this feature.
			- [ ] Optionally set <kbd><var>`PERFORMANCE_DISABLED`</var></kbd> to <kbd><var>`1`</var></kbd> to disable this feature.
			- [ ] Optionally set <kbd><var>`REVIEW_DISABLED`</var></kbd> to <kbd><var>`1`</var></kbd> to disable this feature.
			- [ ] Optionally set <kbd><var>`SAST_DISABLED`</var></kbd> to <kbd><var>`1`</var></kbd> to disable this feature.
			- [ ] Optionally set <kbd><var>`TEST_DISABLED`</var></kbd> to <kbd><var>`1`</var></kbd> to disable this feature.
